const express = require('express'),
    http = require('http'),
    { join } = require('path');

const app = express();

app.use(express.static(__dirname));

const port = process.env.PORT || '8888';
app.set('port', port);

app.get('/', function(req, res) {
    res.sendFile(__dirname + '\\index.html');
});

let server = http.createServer(app);

server.listen(port, function() {
    console.log(`started at: ${port}`);
});