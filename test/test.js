var canvas, ctx;

window.onload = function() {
    canvas = document.querySelector('canvas');
    canvas.style.width = window.innerWidth;
    canvas.style.height = window.innerHeight;

    ctx = canvas.getContext('2d');
    ctx.font = "16px Lato";
};