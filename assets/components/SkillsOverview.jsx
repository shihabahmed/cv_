class SkillsOverview extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            title: this.props.container.dataset.title,
            content: this.props.content
        };
    }

    generateHTML(array) {
        return `${array.map(skill => `<div data-strength='${skill.strength}'>${skill.name}</div>`).join('\n  ')}`;
    }

    rawHTML() {
        return { __html: this.generateHTML(this.state.content) };
    }

    periodicDataLoader() {
        this.setState({
            content: this.props.content
        });
    }

    render() {
        return (
            <div className="skills-overview-component-wrapper react-component-wrapper">
                <h3 className="header">
                    <span>{this.state.title}</span>
                </h3>
                <div className="content">
                    {/* <div class="skills-strength strength-indicator" data-type="dash" data-color="#3ab5ff" data-shape="rectangle"> */}
                    <div className="skills-strength strength-indicator" data-type="dash" data-color="#3ab5ff" dangerouslySetInnerHTML={this.rawHTML()}></div>
                </div>
            </div>
        );
    }

    componentDidMount() {
        componentLoaded();
        // let selector = '.skills-overview-component-wrapper.react-component-wrapper';
        // document.querySelector(selector).outerHTML = document.querySelector(selector).innerHTML;

        if (this.props.autoUpdate) {
            this.timerID = setInterval(
                () => this.periodicDataLoader(),
                1000
            );
        }
    }

    componentDidUpdate() {
        componentLoaded();
    }

    componentWillUnmount() {
        clearInterval(this.timerID);
    }
}

ReactDOM.render(
    <SkillsOverview content={cv.skills.overview} container={document.querySelector('section.skills-overview')} />,
    document.querySelector('section.skills-overview')
);

