class Interests extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            title: this.props.container.dataset.title,
            content: this.props.content
        };
    }

    generateHTML(array) {
        return `${array.map(
            item =>
                `<div class="group">
                    <span class="label">${item.name}</span>
                    <span class="tools">${item.interests}</span>
                </div>`
        ).join('\n  ')}`;
    }

    rawHTML() {
        return { __html: this.generateHTML(this.state.content) };
    }

    periodicDataLoader() {
        this.setState({
            content: this.props.content
        });
    }

    render() {
        return (
            <div className="interests-component-wrapper react-component-wrapper">
                <h3 className="header">
                    <span>{this.state.title}</span>
                </h3>
                <div className="content groups" dangerouslySetInnerHTML={this.rawHTML()}></div>
            </div>
        );
    }

    componentDidMount() {
        componentLoaded();
        // let selector = '.interests-component-wrapper.react-component-wrapper';
        // document.querySelector(selector).outerHTML = document.querySelector(selector).innerHTML;

        if (this.props.autoUpdate) {
            this.timerID = setInterval(
                () => this.periodicDataLoader(),
                1000
            );
        }
    }

    componentDidUpdate() {
        componentLoaded();
    }

    componentWillUnmount() {
        clearInterval(this.timerID);
    }
}

ReactDOM.render(
    <Interests content={cv.interests} container={document.querySelector('section.interests')} />,
    document.querySelector('section.interests')
);

