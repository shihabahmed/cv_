// let Objective = React.createClass({
//     rawMarkup: function(){
//         let rawMarkup = this.props.content;
//         console.log(rawMarkup);
//         rawMarkup = rawMarkup.replace(/class=/g, 'className=');
//         return { __html: rawMarkup };
//     },
//     render: function() {
//         return (
//             <div className="content" content="{cv.objective}" dangerouslySetInnerHTML={this.rawMarkup()}></div>
//         );
//     }
// });

class Objective extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            content: this.props.content
        };
    }

    rawHTML() {
        return { __html: this.state.content };
    }

    periodicDataLoader() {
        this.setState({
            content: this.props.content
        });
    }

    render() {
        return (
            <div className="objective-component-wrapper react-component-wrapper" dangerouslySetInnerHTML={this.rawHTML()}></div>
        );
    }

    componentDidMount() {
        componentLoaded();
        // let selector = '.objective-component-wrapper.react-component-wrapper';
        // document.querySelector(selector).outerHTML = document.querySelector(selector).innerHTML;

        if (this.props.autoUpdate) {
            this.timerID = setInterval(
                () => this.periodicDataLoader(),
                1000
            );
        }
    }

    componentDidUpdate() {
        componentLoaded();
    }

    componentWillUnmount() {
        clearInterval(this.timerID);
    }
}

// document.createElement('objective');

ReactDOM.render(
    <Objective content={cv.objective} />,
    document.querySelector('section.objective')
);

