export class Header extends React.Component {
    rawHTML(){
        var rawMarkup = this.props.content;
        return { __html: rawMarkup };
    }

    render() {
        return (
            <h3 class="header">
                <span>{this.rawHTML()}</span>
            </h3>
        );
    }
}