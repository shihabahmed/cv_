class SummaryOfSkills extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            title: this.props.container.dataset.title,
            content: this.props.content
        };
    }

    generateHTML(array) {
        return `${array.map(item => `<li>${item}</li>`).join('\n  ')}`;
    }

    rawHTML() {
        return { __html: this.generateHTML(this.state.content) };
    }

    // componentWillMount() {
    //     this.setState({title: this.props.container.dataset.title});
    // }

    periodicDataLoader() {
        this.setState({
            content: this.props.content
        });
    }

    render() {
        return (
            <div className="summary-of-skills-component-wrapper react-component-wrapper">
                <h3 className="header">
                    <span>{this.state.title}</span>
                </h3>
                <div className="content">
                    <ul className="summary-of-skills" dangerouslySetInnerHTML={this.rawHTML()}></ul>
                </div>
            </div>
        );
    }

    componentDidMount() {
        componentLoaded();
        // let selector = '.summary-of-skills-component-wrapper.react-component-wrapper';
        // document.querySelector(selector).outerHTML = document.querySelector(selector).innerHTML;

        if (this.props.autoUpdate) {
            this.timerID = setInterval(
                () => this.periodicDataLoader(),
                1000
            );
        }
    }

    componentDidUpdate() {
        componentLoaded();
    }

    componentWillUnmount() {
        clearInterval(this.timerID);
    }
}

ReactDOM.render(
    <SummaryOfSkills content={cv.skills.summary} container={document.querySelector('section.summary-of-skills')} />,
    document.querySelector('section.summary-of-skills')
);

