class Education extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            title: this.props.container.dataset.title,
            content: this.props.content
        };
    }

    generateHTML(array) {
        return `${array.map(
            education =>
                `<div class="degree list-item">
                    <div class="title">${education.degree}</div>
                    ${education.institute}
                </div>`
        ).join('\n  ')}`;
    }

    rawHTML(){
        return { __html: this.generateHTML(this.state.content) };
    }

    periodicDataLoader() {
        this.setState({
            content: this.props.content
        });
    }

    render() {
        return (
            <div className="education-component-wrapper react-component-wrapper">
                <h3 className="header">
                    <span>{this.state.title}</span>
                </h3>
                <div className="content degrees list" dangerouslySetInnerHTML={this.rawHTML()}></div>
            </div>
        );
    }

    componentDidMount() {
        componentLoaded();
        // let selector = '.education-component-wrapper.react-component-wrapper';
        // document.querySelector(selector).outerHTML = document.querySelector(selector).innerHTML;
        
        if (this.props.autoUpdate) {
            this.timerID = setInterval(
                () => this.periodicDataLoader(),
                1000
            );
        }
    }

    componentDidUpdate() {
        componentLoaded();
    }

    componentWillUnmount() {
        clearInterval(this.timerID);
    }
}

ReactDOM.render(
    <Education autoUpdate="false" content={cv.education} container={document.querySelector('section.education')} />,
    document.querySelector('section.education')
);

