class TechnicalSkills extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            title: this.props.container.dataset.title,
            content: this.props.content
        };
    }

    generateHTML(array) {
        return `${array.map(
            item =>
                `<div class="skill group">
                    <span class="label">${item.name}</span>
                    <span class="tools">${item.skills}</span>
                </div>`
        ).join('\n  ')}`;
    }

    rawHTML() {
        return { __html: this.generateHTML(this.state.content) };
    }

    // componentWillMount() {
    //     this.setState({title: this.props.container.dataset.title});
    // }

    periodicDataLoader() {
        this.setState({
            content: this.props.content
        });
    }

    render() {
        return (
            <div className="skills-component-wrapper react-component-wrapper">
                <h3 className="header">
                    <span>{this.state.title}</span>
                </h3>
                <div className="content technical-skills groups" dangerouslySetInnerHTML={this.rawHTML()}></div>
            </div>
        );
    }

    componentDidMount() {
        componentLoaded();
        // let selector = '.skills-component-wrapper.react-component-wrapper';
        // document.querySelector(selector).outerHTML = document.querySelector(selector).innerHTML;

        if (this.props.autoUpdate) {
            this.timerID = setInterval(
                () => this.periodicDataLoader(),
                1000
            );
        }
    }

    componentDidUpdate() {
        componentLoaded();
    }

    componentWillUnmount() {
        clearInterval(this.timerID);
    }
}

ReactDOM.render(
    <TechnicalSkills content={cv.skills.technical} container={document.querySelector('section.skills')} />,
    document.querySelector('section.skills')
);

