class Employment extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            title: this.props.container.dataset.title,
            content: this.props.content
        };
    }

    generateHTML(array) {
        return `${array.map(
            employment =>
                `<div class="job">
                    <div class="company">${employment.company}</div>
                    <div class="employments">
                        ${employment.history.map(
                            history =>
                                `<div class="employment">
                                    <span class="designation">${history.designation}</span>
                                    <div class="responsibility">
                                        <ul>
                                            ${history.responsibilities.map(
                                                responsibility =>
                                                    `<li>${responsibility}</li>`
                                            ).join('\n      ')}
                                        </ul>
                                    </div>
                                </div>`
                        ).join('\n    ')}
                    </div>
                </div>`
            ).join('\n  ')}`;
    }

    rawHTML() {
        return { __html: this.generateHTML(this.state.content) };
    }

    periodicDataLoader() {
        this.setState({
            content: this.props.content
        });
    }

    render() {
        return (
            <div className="employment-history-component-wrapper react-component-wrapper">
                <h3 className="header">
                    <span>{this.state.title}</span>
                </h3>
                <div className="content jobs" dangerouslySetInnerHTML={this.rawHTML()}></div>
            </div>
        );
    }

    componentDidMount() {
        componentLoaded();
        // let selector = '.employment-history-component-wrapper.react-component-wrapper';
        // document.querySelector(selector).outerHTML = document.querySelector(selector).innerHTML;

        if (this.props.autoUpdate) {
            this.timerID = setInterval(
                () => this.periodicDataLoader(),
                1000
            );
        }
    }

    componentDidUpdate() {
        componentLoaded();
    }

    componentWillUnmount() {
        clearInterval(this.timerID);
    }
}

ReactDOM.render(
    <Employment content={cv.employment} container={document.querySelector('section.employment-history')} />,
    document.querySelector('section.employment-history')
);

