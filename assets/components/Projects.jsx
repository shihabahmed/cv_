class Projects extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            title: this.props.container.dataset.title,
            content: this.props.content
        };
    }

    generateHTML(array) {
        return `${array.map(
            item => 
                `<div class="project list-item">
                    <div class="title">
                        ${item.name}
                        ${item.url.length > 0 ? `<span class="project-link"><a href="//${item.url}" target="_blank">${item.url}</a></span>` : ''}
                    </div>
                    ${item.desc.length > 0 ? `<div class="detail">${item.desc}</div>` : ''}
                    ${item.task.length > 0 ? `<div class="responsibility">${item.task}</div>` : ''}
                </div>`
            ).join('\n  ')}`;
    }

    rawHTML() {
        return { __html: this.generateHTML(this.state.content) };
    }

    periodicDataLoader() {
        this.setState({
            content: this.props.content
        });
    }

    render() {
        return (
            <div className="projects-component-wrapper react-component-wrapper">
                <h3 className="header">
                    <span>{this.state.title}</span>
                </h3>
                <div className="content projects list" dangerouslySetInnerHTML={this.rawHTML()}></div>
            </div>
        );
    }

    componentDidMount() {
        componentLoaded();
        // let selector = '.projects-component-wrapper.react-component-wrapper';
        // document.querySelector(selector).outerHTML = document.querySelector(selector).innerHTML;

        if (this.props.autoUpdate) {
            this.timerID = setInterval(
                () => this.periodicDataLoader(),
                1000
            );
        }
    }

    componentDidUpdate() {
        componentLoaded();
    }

    componentWillUnmount() {
        clearInterval(this.timerID);
    }
}


ReactDOM.render(
    <Projects content={cv.projects.portfolio} container={document.querySelector('section.portfolio')} />,
    document.querySelector('section.portfolio')
);

ReactDOM.render(
    <Projects content={cv.projects.personal} container={document.querySelector('section.personal-projects')} />,
    document.querySelector('section.personal-projects')
);
