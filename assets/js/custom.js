var body, welcomeMessage, themeChanger, nav, navContainer, container, header, contentArea, sections, sly;

var defaultWelcomeTransitionDelay = 500;

var fn = {
    set: function(data) {
        if (data) {
            var siteData = JSON.parse(localStorage.getItem('shihabahmedbhuiyan') || '{}');
            siteData = $.extend(true, {}, siteData, data);
            localStorage.setItem('shihabahmedbhuiyan', JSON.stringify(siteData));
        }
    },

    get: function(key) {
        var siteData = JSON.parse(localStorage.getItem('shihabahmedbhuiyan') || '{}');
        if (key) {
            return siteData[key];
        }
    },

    changeTheme: function() {
        if (body.hasClass('dark')) {
            body.removeClass('dark');
            fn.set({ theme: 'light' });
        } else {
            body.addClass('dark');
            fn.set({ theme: 'dark' });
        }
    },

    animate: function(el, animation, callback) {
        el.removeClass('hidden fadeInUp').addClass('animated ' + animation);
        if (callback && typeof callback === 'function') {
            el.one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', callback);
        }
    },

    hideWithAnimation: function(el, callback, delay) {
        setTimeout(function() {
            fn.animate(el, 'fadeOut', function() {
                el.removeClass('animated, fadeOut').addClass('hidden');
            });
            if (callback && typeof callback === 'function') {
                callback();
            }
        }, delay || defaultWelcomeTransitionDelay);
    },

    showWelcome: function() {
        welcomeMessage = $('.welcome-message').animate({
            opacity: 1
        }, 400, function() {
            $('body').children('div.themer-container, div.nav-container, div.wrapper').css('opacity', 1);
        });
        var welcome = {
            hi: welcomeMessage.find('.hi'),
            intro: welcomeMessage.find('.intro'),
            cv: welcomeMessage.find('.cv'),
            fn: {
                hiAnimated: function() {
                    fn.hideWithAnimation(welcome.hi, function() {
                        fn.animate(welcome.intro, 'fadeIn', welcome.fn.introAnimated);
                    });
                },
                introAnimated: function() {
                    fn.hideWithAnimation(welcome.intro, function() {
                        fn.animate(welcome.cv, 'fadeIn', welcome.fn.cvAnimated);
                    });
                },
                cvAnimated: function() {
                    fn.hideWithAnimation(welcome.cv, function() {
                        welcomeMessage.css('top', '100%');
                        setTimeout(function() {
                            body.removeClass('blur');
                        }, 800);
                        setTimeout(function() {
                            welcomeMessage.remove();
                        }, 2000);
                    });
                }
            }
        };
        fn.animate(welcome.hi, 'fadeIn', welcome.fn.hiAnimated);
    },

    dateDiff: function(date1, date2) {
        var diff = (date2 - date1) / 31449600000;
        diff = diff < 1 ? -(diff) : diff;

        var intDiff = parseInt(diff),
            fraction = diff - intDiff;

        if (fraction > 0.8) {
            return 'Nearly ' + (intDiff + 1);
        } else if (fraction >= 0.25) {
            return intDiff + '+';
        } else {
            return intDiff;
        }
    },

    scrollTo: function(section, scrollPos, callback) {
        var runCallback = function() {
            if (callback && typeof callback === 'function') {
                callback(section);
            }
        };

        var slyFrame = section ? section.closest('.sly-frame') : null;
        if (slyFrame && slyFrame.length > 0) {
            sly = slyFrame.data().sly;
            if (sly) {
                var isMoving = false;
                sly.on('moveStart', function() {
                    isMoving = true;
                });
                sly.toCenter(section);
                sly.one('moveEnd', function() {
                    isMoving = false;
                    runCallback();
                });
                if (!isMoving) runCallback();
            }
        } else {
            slyFrame = body.find('.main-content .sly-frame');
            if (slyFrame && slyFrame.length > 0 && scrollPos === 0) {
                slyFrame.data().sly.toStart();
            } else {
                $('html, body').animate({
                    scrollTop: scrollPos
                }, 500, runCallback);
            }
        }
    },

    highlightSection: function(sectionSelector, link) {
        setTimeout(function() {
            //history.pushState({}, '', sectionSelector);
            sectionSelector = '.' + sectionSelector.substring(1);

            var section = body.find('section').filter(sectionSelector),
                sectionData = {
                    position: section.offset(),
                    size: {
                        height: section.height() + 20
                    }
                },
                windowData = {
                    height: window.innerHeight
                },
                scrollSize = (windowData.height - sectionData.size.height) / 2;

            scrollSize = (scrollSize < 10 ? 10 : scrollSize) + header.outerHeight(true) + 5;

            fn.scrollTo(section, (sectionData.position.top - scrollSize), function(activeSection) {
                body.addClass('section-highlighted').find('section.highlight').removeClass('highlight');
                activeSection.addClass('highlight');
                setTimeout(function() {
                    activeSection.removeClass('highlight');
                    body.removeClass('section-highlighted');
                }, 5000);
            });
        }, 400);
    },

    toggleNav: function() {
        if (navContainer.hasClass('open')) {
            navContainer.children('.bg').fadeOut();
            body.removeClass('content-blur');
            nav.css('left', -nav.outerWidth());
            setTimeout(function() {
                navContainer.removeClass('open');
            }, 400);
        } else {
            navContainer.addClass('open').children('.bg').fadeIn();
            body.addClass('content-blur');
            nav.css('left', 0);
        }
    },

    addHeader: function() {
        var bodyContent = $('.body-content'),
            personalInfo = bodyContent.find('.personal-info img.photo, .personal-info .info h3').clone();

        header = $('<header class="mobile"></header>');

        header
            .append(personalInfo.filter('img').removeClass('photo').addClass('thumbnail'))
            .append(personalInfo.filter('h3'));
        bodyContent.prepend(header);
    },

    getHeaderThreshold: function() {
        var name = container.find('.info h3');
        if (window.innerWidth < 641) {
            return name.offset().top - ((45 - name.height()) / 2);
        } else {
            return window.outerHeight;
        }
    },

    initBookmarks: function() {
        var bookmarks = '<h4><em class="ico ico-arrow-down"></em> Jump To <em class="ico ico-arrow-down"></em></h4><div class="bookmarks">',
            sectionData = {};
        for (var i = 0, length = sections.length; i < length; i++) {
            sectionData = sections.eq(i).data();
            bookmarks += '<a href="#' + sectionData.id + '">' + sectionData.title + '</a>';
        }
        bookmarks += '</div>';

        nav.html(bookmarks).append('<span class="handle pulse"> . . . </span>').css('left', -nav.outerWidth());

        nav.delegate('a', 'click', function(e) {
            e.preventDefault();
            if ($(this).hasClass('active')) {
                return false;
            } else {
                fn.highlightSection($(this).attr('href'), $(this));
            }
        });
    },

    setContentAreaHeight: function() {
        if (window.innerWidth > 640) {
            contentArea.css('height', (window.innerHeight - $('footer').outerHeight(true)));
            fn.sly.reload();
        } else {
            contentArea.css('height', null);
            fn.sly.destroy();
        }
    },

    updateExternalLinks: function() {
        $('a[href][target="_blank"]').not('.cloned').each(function(index, el) {
            $(el).addClass('cloned').after($(el).clone().addClass('external-link').attr('title', 'Open in new tab').html('<em class="ico ico-forward"></em>')).removeAttr('target');
        });
    },

    sly: {
        init: function() {
            contentArea.each(function() {
                var el = $(this),
                    slyFrame = el.find('.sly-frame'),
                    scrollbar;
                if (slyFrame.length === 0) {
                    el.children().wrapAll('<div class="sly-frame"><div class="slidee"></div></div>');
                    slyFrame = el.find('.sly-frame').after('<div class="scrollbar"><div class="handle"></div></div>');
                    scrollbar = slyFrame.siblings('.scrollbar');
                }

                slyFrame.sly({
                    mouseDragging: 0,
                    touchDragging: 1,
                    releaseSwing: 1,
                    elasticBounds: 1,
                    // scrollTrap: 0,
                    speed: 300,
                    activatePageOn: 'click',
                    scrollBar: scrollbar,
                    scrollBy: 100,
                    dragHandle: 1,
                    dynamicHandle: 1,
                    clickBar: 1
                }, {
                    load: function(e) {
                        if (scrollbar.height() === scrollbar.find('.handle').height()) {
                            scrollbar.hide();
                        } else {
                            scrollbar.show();
                        }
                    }
                });
            });
        },
        reload: function() {
            var slyFrame = contentArea.find('.sly-frame');
            if (slyFrame.length > 0) {
                slyFrame.each(function() {
                    sly = $(this).data().sly;
                    if (sly)
                        sly.reload();
                    else
                        fn.sly.init();
                });
            } else {
                fn.sly.init();
            }
        },
        destroy: function() {
            contentArea.find('.sly-frame').each(function() {
                var el = $(this).sly('destroy');
                el.siblings('.scrollbar').remove();
                el.parent().html(el.find('.slidee').html());
            });
        }
    },

    init: function() {
        fn.setContentAreaHeight();
        fn.initBookmarks();
        fn.addHeader();
    }
};

var componentLoaded = function() {
    StrengthIndicator.init();
    fn.updateExternalLinks();
    fn.setContentAreaHeight();
};

(function($) {
    $(function() {
        fn.showWelcome();
        // setTimeout(function() {
        //     $('body')
        //         .removeClass('blur')
        //         .children('div.themer-container, div.nav-container, div.wrapper').css('opacity', 1)
        //         .end().children('.welcome-message').remove();
        // }, 500);

        body = $('body');
        var theme = fn.get('theme');
        if (theme == 'dark') {
            body.addClass('dark');
        } else {
            body.removeClass('dark');
        }

        nav = body.find('nav').wrap('<div class="nav-container"></div>');
        navContainer = nav.parent().prepend('<div class="bg"></div>');
        container = body.find('.container');
        contentArea = container.find('.content-area');
        sections = contentArea.find('section');

        var jobStartDate = new Date(2009, 2, 23),
            currentDate = new Date();

        var headerThreshold = fn.getHeaderThreshold();

        // $('.wrapper').on('mousewheel', function(e) {
        //     if (navContainer.hasClass('open')) {
        //         e.preventDefault();
        //         e.stopPropagation();
        //     }
        // });

        body.delegate('.theme-changer', 'click', function() {
            fn.changeTheme();
        });

        navContainer.children('.bg').click(fn.toggleNav);

        fn.updateExternalLinks();

        nav.delegate('.bookmarks a, .handle', 'click', fn.toggleNav);

        setInterval(function() {
            sections.find('.yearsOfExperience').html(fn.dateDiff(currentDate, jobStartDate));
        }, 1000);

        var sectionId = window.location.hash;

        window.onload = function() {
            if (sectionId && sectionId.length > 0)
                fn.highlightSection(sectionId, nav.find('a[href="' + sectionId + '"]'));

            if (window.innerWidth > 640)
                fn.sly.init();
        };

        window.onresize = function() {
            fn.setContentAreaHeight();
            headerThreshold = fn.getHeaderThreshold();
        };

        window.onscroll = function() {
            if (window.scrollY >= headerThreshold) {
                header.show();
            } else {
                header.hide();
            }
        };

        fn.init();
    });
})(jQuery);
