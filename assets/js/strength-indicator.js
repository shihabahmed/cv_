var StrengthIndicator = {};

(function($) {
    $.fn.extend({
        strengthIndicator: function() {
            var style = {
                default: '<style>.strength-wrapper{height:5px;border-radius:10px;background: #e61f00;background: -moz-linear-gradient(left, #e61f00 0%, #c9cc00 50%, #00c200 100%);background: -webkit-gradient(left top, right top, color-stop(0%, #e61f00), color-stop(50%, #c9cc00), color-stop(100%, #00c200));background: -webkit-linear-gradient(left, #e61f00 0%, #c9cc00 50%, #00c200 100%);background: -o-linear-gradient(left, #e61f00 0%, #c9cc00 50%, #00c200 100%);background: -ms-linear-gradient(left, #e61f00 0%, #c9cc00 50%, #00c200 100%);background: linear-gradient(to right, #e61f00 0%, #c9cc00 50%, #00c200 100%);filter: progid:DXImageTransform.Microsoft.gradient( startColorstr="#e61f00", endColorstr="#00c200", GradientType=1 );}.bar{height:100%;background-color:#e4e4e4;float:right;}</style>',
                dash: '<style>.strength-wrapper{height:5px;margin:0 -1px}.strength-wrapper:after{content:"";display:block;clear:both;}.bar{box-sizing:border-box;float:left;width:5%;height:100%;overflow:hidden;position:relative;}.bar:nth-child(2n){padding-right: 1px;}.bar:nth-child(2n+1){padding-left: 1px;}.bar:not(.rectangle):after{content:"";display:block;position:absolute;border:5px solid transparent;width:10px;height:10px;}.bar:nth-child(2n):after{border-bottom-color:#fff;bottom:0px;right:-5px;}.dark .bar:nth-child(2n):after{border-bottom-color:#383838;}.bar:nth-child(2n+1):after{border-top-color:#fff;top:0px;left:-5px;}.dark .bar:nth-child(2n+1):after{border-top-color:#383838;}.dash{height:100%;}</style>'
            };

            return this.each(function() {
                var data = $(this).data();

                $(this).children().not('.strength-indicated').each(function(index, el) {
                    var options = {
                        type: 'bar',
                        shape: '', // 'rectangle'
                        color: '#009fff'
                    };

                    var skill = $(this).addClass('strength-indicated').css('cursor', 'default'),
                        skillData = skill.data(),
                        strength = skillData.strength,
                        strengthWrapper = $('<div class="strength-wrapper"></div>'),
                        strengthBar = '';

                    options = $.extend(true, {}, options, data);

                    if (options.type === 'dash') {
                        if (index === 0) {
                            $('head').append(style.dash);
                        }
                        strengthBar = $('<div class="bar ' + options.shape + '"></div>');

                        for (var i = 1, l = parseInt(skillData.strength / 5), total = 20; i <= total; i++) {
                            if (i <= l) {
                                strengthWrapper.append(strengthBar.clone().html('<div class="dash" style="background-color:' + options.color + '"></div>'));
                            } else {
                                strengthWrapper.append(strengthBar.clone().html('<div class="dash" style="background-color:rgba(125, 125, 125, 0.3)"></div>'));
                            }
                        }
                    } else {
                        if (index === 0) {
                            $('head').append(style.default);
                        }
                        strengthBar = $('<div class="bar"></div>');
                        strengthWrapper.append(strengthBar);

                        var c1 = parseInt(skillData.strength),
                            c2 = 100 - c1;

                        strength = 100 - strength;
                        strength = strength === 100 ? 99.5 : strength;

                        strengthBar.css({
                            //'background-color': 'rgb(' + Math.ceil(255 * c2 / 100) + ',' + Math.floor(225 * c1 / 100) + ',00)',
                            'width': strength + '%'
                        });
                    }

                    skill.append(strengthWrapper).attr('title', 'Strength: ' + skillData.strength + '%');
                });
            });
        }
    });

    $(function() {
        StrengthIndicator = {
            init: function() {
                $('.strength-indicator').strengthIndicator();
            }
        };

        StrengthIndicator.init();
    });
})(jQuery);