var cv = {
    objective: "With a strong technical skill-set, attention to detail, and <span class=\"yearsOfExperience\"></span> years of experience, I want to work as a software developer where I could create digital magic and elevate user experience to the next level.",
    skills: {
        overview: [{
                name: "HTML5",
                strength: "75"
            },
            {
                name: "CSS3",
                strength: "85"
            },
            {
                name: "jQuery",
                strength: "95"
            },
            {
                name: "ES6",
                strength: "75"
            },
            {
                name: "TypeScript",
                strength: "50"
            },
            {
                name: "node.js",
                strength: "75"
            },
            {
                name: "ReactJs",
                strength: "30"
            },
            {
                name: "SQL",
                strength: "70"
            },
            {
                name: "MongoDB",
                strength: "65"
            },
            {
                name: "Photoshop",
                strength: "70"
            }
        ],
        summary: [
            "<span class=\"yearsOfExperience\">Calculating</span> years of experience in web development.",
            "Executed and contributed to full-stack web development projects, with an emphasis on front-end features, browser manipulation and cross-browser compatibility.",
            "Assisted in development of back-end features in C#.",
            "Experienced in creating custom js/jquery plugins.",
            "Strong understanding of industry trends.",
            "Skilled in multitasking and working on several projects simultaneously.",
            "Troubleshoot problems with unique ability to improvise solutions.",
            "Experienced in communicating with the creative team and clients.",
            "Passionate about adopting ideas and bringing them to life through technology.",
            "Well-organized with an ability to prioritize tasks.",
            "Ability to work under pressure with strict deadline.",
            "Ability to manage teams."
        ],
        technical: [{
            name: "Frontend",
            skills: "HTML5, JavaScript, ReactJS, TypeScript"
        }, {
            name: "UI/UX",
            skills: "jQuery, Bootstrap, CSS3, SCSS, Stylus"
        }, {
            name: "Backend",
            skills: "ASP.Net, node.js, express.js, socket.io"
        }, {
            name: "DB",
            skills: "SQL Server, MySQL, PostGRE, MongoDB, SQLite"
        }, {
            name: "Mobile",
            skills: "Ionic framework, Cordova"
        }, {
            name: "Versioning",
            skills: "Git, Subversion"
        }, {
            name: "Tools",
            skills: "Photoshop, Visual Studio, VS Code, Sublime, GitKraken, SourceTree, Tortoise"
        }]
    },
    projects: {
        portfolio: [{
            name: "SilentAction",
            url: "www.silentaction.com",
            desc: "SilentAction was created to give the tools needed to run a successful online fundraising campaign.",
            task: "Co-designed this theme with inspiration from other fund-raising sites, and then developed from scratch."
        }, {
            name: "Strataspot",
            url: "",
            desc: "An efficient and communicative tool for building management.",
            task: "This is a huge application where I have dealt with all the frontend needs. Used <a href=\"http://keenthemes.com/preview/metronic/\" target=\"_blank\">metronic</a> as the theme base, then molded according to the client's requirements."
        }, {
            name: "Asset Register",
            url: "",
            desc: "An efficient and communicative tool for managing assets within the premises.",
            task: "Revamped the UI for enhanced visual experience and usability."
        }, {
            name: "FrankyApp",
            url: "",
            desc: "An iPad-based ordering system for restaurants.",
            task: "Co-developed this application using HTML, CSS and JS. Built using the ionic framework."
        }],
        personal: [{
            name: "ChatNinja",
            url: "chatninja.herokuapp.com",
            desc: "Simple chat application built using Node.js, express.js and socket.io.",
            task: ""
        }, {
            name: "jQuery File Manager",
            url: "filemgr.herokuapp.com",
            desc: "A jquery plugin for rendering a view of folders and files similar to windows explorer.",
            task: ""
        }]
    },
    interests: [{
        name: "Technical",
        interests: "AngularJS, ReactJS, <small class=\"smaller\">React Native, Python, Ruby on Rails</small>"
    }, {
        name: "Other",
        interests: "Movies, Gadgets, Technology etc..."
    }],
    employment: [{
        company: "Astha IT Research &amp; Consultancy Ltd.",
        history: [{
            designation: "Project Manager <small>[Since Apr, 2015]</small>",
            responsibilities: [
                "Ensure best UX for the visitors of our products.",
                "Manage multiple projects.",
                "Review and re-factor source codes written by other developers.",
                "Communicate with customers."
            ]
        }, {
            designation: "Software Engineer <small>[Apr, 2012 - Apr, 2015]</small>",
            responsibilities: [
                "Prepare frontend for all projects from scratch with best practices.",
                "Work with the backend developers.",
                "Ensure best UX for the visitors of our products."
            ]
        }]
    }, {
        company: "FuseboxDesign Ltd, UK",
        history: [{
            designation: "Web Developer <small>[Mar, 2009 – Feb, 2012]</small>",
            responsibilities: [
                "Transform customer requirements to technical implementation.",
                "Work as part of a team of other developers and designers.",
                "Deliver quality products ensuring customer satisfaction even under strict deadline.",
                "Communicate with customers."
            ]
        }]
    }],
    education: [{
        degree: "MSc in Computing (2008)",
        institute: "<a class=\"institute\" href=\"//www.tees.ac.uk/\" target=\"_blank\">University of Teesside</a>, UK"
    }, {
        degree: "BSc in Computing and Information Systems (2006)",
        institute: "<a class=\"institute\" href=\"//www.diit.info/\" target=\"_blank\">Daffodil Institute of IT (DIIT)</a>, Bangladesh"
    }]
}