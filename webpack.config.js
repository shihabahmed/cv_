module.exports = {
    devServer: {
        contentBase: __dirname,
        compress: true,
        port: 9000
    }
};